import ApiService from './base';

export const getAllCart = async () => {
  const url = `/getAllCart`
  return ApiService.post(url, {number: 999999})
}

export const updateStatus = async (data) => {
  const url = `/updateStatus`
  return ApiService.post(url, data)
}

export const getCart = async (id) => {
  const url = `/getCart`
  return ApiService.post(url, {id})
}

// const getUid = () => {
//   let uid = null;
//   try{
//     const token = JSON.parse(localStorage.getItem("TOKEN")) || {};
//     uid = token.localId || null;
//   }catch(e){console(e)}
//   return uid;
// }
