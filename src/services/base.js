import axios from "axios";
import VueAxios from "vue-axios";
import { API_URL } from "./config";
import Vue from 'vue'
Vue.use(VueAxios, axios);

const ApiService = {
  init() {
    Vue.axios.defaults.baseURL = API_URL;
    Vue.axios.defaults.timeout = 60 * 1000;
    Vue.axios.defaults.withCredentials = true;
    Vue.axios.defaults.headers.common[ "Access-Control-Allow-Origin"] = '*';

    Vue.axios.interceptors.response.use(
      (response) => {  
        return response.data
       },
      (error) => {
        if (error.request.status === 401){
          localStorage.removeItem('TOKEN')
          Vue.$router.push({name: 'login'})     
        }
        throw error
      }
    )
  },

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  get(resource, prms) {
    if(prms) {
      return Vue.axios.get(`${resource}`, prms)
    } else {
      return Vue.axios.get(`${resource}`)
    }
   
  },

  upload(resource, params,data){
    return Vue.axios(`${resource}`, {
          method: "POST",
          params:  params,
          data: data,
          headers: {
            Accept: "application/json",
            ContentType:" multipart/form-data"
          },
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params)
      // .then(res => {
      //   if(res.data.httpStatusErrorCode)
      //     return Promise.reject({response: res})
      //   return res
      // });
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource, params = {}) {
    return Vue.axios.delete(resource, params)
  }
};

export default ApiService;