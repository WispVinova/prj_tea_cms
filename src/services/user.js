import ApiService from './base';

export const getAllUser = async () => {
  const url = `/getAllUser`
  return ApiService.post(url)
}

export const deleteUser = async (uid) => {
  const url = `/deleteUser`
  return ApiService.post(url, {uid})
}