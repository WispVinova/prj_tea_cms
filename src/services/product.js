import ApiService from './base';

export const getAllProduct = async () => {
  const url = `/getAllProduct`
  return ApiService.post(url)
}

export const deleteProduct = async (id) => {
  const url = `/deleteProduct`
  return ApiService.post(url, {id})
}


export const updateProduct = async (data) => {
  const url = `/updateProduct`
  return ApiService.post(url, data)
}

export const getProduct = async (id) => {
  const url = `/getProduct`
  return ApiService.post(url, {id})
}

export const createProduct = async (data) => {
  const url = `/createProduct`
  return ApiService.post(url, data)
}


